source ~/opt/scripts/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle heroku
antigen bundle pip
antigen bundle lein
antigen bundle command-not-found

# haxe completion plugins
antigen bundle tong/zsh.plugin.haxe
antigen bundle tong/zsh.plugin.neko
antigen bundle tong/zsh.plugin.hashlink
antigen bundle tong/zsh.plugin.haxelib

antigen bundle unixorn/autoupdate-antigen.zshplugin
antigen bundle MichaelAquilina/zsh-you-should-use

antigen bundle bryanculver/workon.plugin.zsh

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting

# Load the theme.
antigen theme robbyrussell

# Tell Antigen that you're done.
antigen apply

# vi keys
bindkey -v

# escape lag will be 0.1 seconds instead of the default 0.4
export KEYTIMEOUT=1

# vi mode widget
function zle-line-init zle-keymap-select {
	vim_prompt="%{$fg_bold[yellow]%} [% normal]% %{$reset_color%}"
	rps1="${${keymap/vicmd/$vim_prompt}/(main|viins)/} $eps1"
	zle reset-prompt
}

# enable vi mode widget
zle -N zle-line-init
zle -N zle-keymap-select

export TERM="xterm-256color"

alias .me="cd ~/opt/git/dotfiles"
alias hme="cd ~/opt/dev/heaps"
alias bgnice="nice -n 19 ionice -c2 -n7"
alias duh='du --max-depth=1 -h | sort -h'
alias entr_cargo_test="ls -d * | entr -c cargo test"
alias entr_firefox_refresh="ls *.html | entr reload-browser 'Firefox Nightly'"
alias feh_g="feh -Z -g 500x500 "
alias feh_t="feh -t -p -Z -Ssize -E 200 -y 200 -W 960 --index-info '' --title 'feh' "
alias gcm='git commit -m '
alias gds='git diff --staged'
alias gme="nvim ~/.zprezto/modules/git/alias.zsh"
alias l='ls -CF'
alias la='ls -A'
alias ll='ls -lahF'
alias lls='ls -lahFSr'
alias rsme="cd ~/opt/dev/rust"
alias rustfmt="rustup run nightly rustfmt"
alias v='nvim'
alias vrc='nvim ~/.config/nvim/init.vim'
alias youtube-dl-playlist-continue="youtube-dl -c -o '%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' "
alias youtube-dl-playlist="youtube-dl -o '%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' "
alias zrc='nvim ~/.zshrc'
alias acme='acme -F xos4\ Terminus -W 1916x1041'
alias wo='workon'
alias swo='startworkon'

alias gtd='emacs /mnt/Documents/org/tasks.org'

xo() {
	echo $(xclip -o);
}

unspace() {
	for f in *\ *; do mv "$f" "${f// /_}"; done
}

# Inferno
export EMU="-r$HOME/opt/inferno -c0 -g1916x1041"
infwm() {
	$HOME/opt/inferno/Linux/386/bin/emu $* /dis/sh.dis -c "bind '#U*/home' /usr; svc/net; wm/wm wm/logon -u $USER"
}

infemu() {
	$HOME/opt/inferno/Linux/386/bin/emu $* /dis/sh.dis -c "bind '#U*/home' /usr; svc/net; wm/wm wm/logon"
}

emu_server() {
	/usr/local/inferno/Linux/386/bin/emu -r/usr/local/inferno -c0 -g1916x1041
}

# plan9port
alias acme="acme -W1916x1041"
export PLAN9=/usr/lib/plan9/

# working copy (w)
alias gws='git status'
alias gwd='git diff --no-ext-diff'
alias gwD='git diff --no-ext-diff --word-diff'

# index (i)
alias gid='git diff --no-ext-diff --cached'
alias giD='git diff --no-ext-diff --cached --word-diff'

# Commit (c)
alias gco='git checkout'

# custom
alias glog='git log --oneline --decorate --all --graph'

alias xargo='RUST_TARGET_PATH=$PWD xargo'

# enable color support
if [ -x /usr/bin/dircolors ]; then
	test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
	alias ls='ls --color=auto'
	alias grep='grep --color=auto'
fi

export ANDROID_HOME=$HOME/opt/android-sdk
export PATH="$HOME/.cargo/bin:$PATH"
export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src"

export PATH
PATH=$PATH:$ANDROID_HOME/platform-tools:$ANDROID_HOME/tools
# PATH=$PATH:$ECLIPSE_HOME
PATH=$PATH:$HOME/opt/bin
PATH=$PATH:$HOME/.local/bin
PATH=$PATH:$HOME/opt/krita
PATH=$PATH:/sbin
PATH=$PATH:/usr/local/bin
PATH=$PATH:/opt/devkitpro/devkitARM/bin

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
export WINEPREFIX=$HOME/.config/wine/
export WINEARCH=win32

export DEVKITPRO=/opt/devkitpro
export DEVKITARM=${DEVKITPRO}/devkitARM

export SDL3DS=~/opt/git/SDL-3DS/SDL-1.2.15/include

echo
fortune
echo

VISUAL=nvim
export VISUAL

export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig

export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

is_in_path () {
  type -p $1 >/dev/null 2>/dev/null
}

if (is_in_path urxvt); then
	export TERMINAL="urxvt"
elif (is_in_path roxterm); then
	export TERMINAL="roxterm"
elif (is_in_path konsole); then
	export TERMINAL="konsole"
fi

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

export FZF_DEFAULT_COMMAND='rg --files --follow 2> /dev/null'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

#!/usr/bin/env zsh
#https://gist.github.com/chew-z/db310b0e3184ae335e24d2ec7087a123
# # Ctrl-P - cd to dir or edit file with fzf
# Put in /usr/local/share/zsh/site-functions/
# and add to .zshrc
# source /usr/local/share/zsh/site-functions/ctrl-p
# inspired by
# https://adamheins.com/blog/ctrl-p-in-the-terminal-with-fzf


fzf-edit-file-or-open-dir() {
    local out key file helpline
    helpline="Ctrl-f to reveal in Finder | Enter to edit file"
    # IFS=$'\n' out=($(ag -g "" | fzf --header="$helpline" \
    # IFS=$'\n' out=($(bfs | fzf --header="$helpline" \
    IFS=$'\n' out=($(rg --files --hidden --glob "!.git/*" | fzf --header="$helpline" \
            --exit-0 \
            --expect=ctrl-f \
            --preview '[ -f {} ] && head -n 50 {}' \
            --preview-window down:4 \
            --bind='?:toggle-preview' ))
    key=$(head -1 <<< "$out")
    file=$(head -2 <<< "$out" | tail -1)

    if [ "$key" = ctrl-f ]; then
        open -R "$file" # reveal in Finder
    else
        if [ -f "$file" ]; then
            # /usr/local/bin/mvim "$file"
            nvim "$file"
        elif [ -d "$file" ]; then
            cd "$file"
        fi
        zle reset-prompt
    fi
    zle accept-line
}
zle     -N   fzf-edit-file-or-open-dir
bindkey '^P' fzf-edit-file-or-open-dir

echo "\nworkon:"
ls ~/.workon_projects
echo ""

export PATH=$PATH:/usr/lib/plan9/bin
export PATH=~/opt/haxe_20180612063724_1e3e5e0/:$PATH

# Sorts uppercase before lower case and others
export LC_ALL=C.UTF-8

hen () {
	pkill hl
	date --iso-8601=seconds
	echo "building..."
	find src/ | entr -c -s ' \
		date --iso-8601=seconds && \
		echo "building..." && \
		haxe hl.hxml && \
		echo "building js..." && \
		haxe js.hxml && \
		echo "running..." && \
		hl /tmp/game.hl && \
		echo "Done." \
		'
}

