#!/usr/bin/env zsh

if [[ ! -d ~/.local/share/nvim/site/autoload/plug.vim ]]; then
	curl -flo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
	    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

if [[ ! -d ~/.zprezto ]]; then
	# install prezto
	git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"

	setopt EXTENDED_GLOB
	for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do
		ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
	done
fi

if [[ ! -d ~/opt/scripts ]]; then
	mkdir -p ~/opt/scripts
	curl -L git.io/antigen > ~/opt/scripts/antigen.zsh
fi

mkdir -p ~/.config/nvim
mkdir ~/.config/git
mkdir ~/.config/nvim/undodir
mkdir ~/.config/nvim/backup
mkdir ~/.config/nvim/swap
mkdir ~/.config/nvim/colors
mkdir ~/.config/nvim/session
mkdir ~/.config/nvim/autoload
mkdir ~/.config/nvim/ftdetect
mkdir ~/.config/nvim/syntax

mkdir -p ~/.config/i3
mkdir -p ~/opt/bin

ln  -i  -s  $PWD/Xmodmap                         ~/.Xmodmap
ln  -i  -s  $PWD/Xresources                      ~/.Xdefaults
ln  -i  -s  $PWD/Xresources                      ~/.Xresources
ln  -i  -s  $PWD/agignore                        ~/.agignore
ln  -i  -s  $PWD/config                          ~/.config/i3/config
ln  -i  -s  $PWD/ctags                           ~/.ctags
ln  -i  -s  $PWD/i3status.conf                   ~/.i3status.conf
ln  -i  -s  $PWD/ignore                          ~/.config/git/ignore
ln  -i  -s  $PWD/nvim/DumbLine.vim               ~/.config/nvim/autoload/DumbLine.vim
ln  -i  -s  $PWD/nvim/MyFunctions.vim            ~/.config/nvim/autoload/MyFunctions.vim
ln  -i  -s  $PWD/nvim/OthersFunctions.vim        ~/.config/nvim/autoload/OthersFunctions.vim
ln  -i  -s  $PWD/nvim/ftdetect/infsh.vim         ~/.config/nvim/ftdetect/infsh.vim
ln  -i  -s  $PWD/nvim/ftdetect/limbo.vim         ~/.config/nvim/ftdetect/limbo.vim
ln  -i  -s  $PWD/nvim/ftdetect/mkfile.vim        ~/.config/nvim/ftdetect/mkfile.vim
ln  -i  -s  $PWD/nvim/init.vim                   ~/.config/nvim/init.vim
ln  -i  -s  $PWD/nvim/syntax/infsh.vim           ~/.config/nvim/syntax/infsh.vim
ln  -i  -s  $PWD/nvim/syntax/limbo.vim           ~/.config/nvim/syntax/limbo.vim
ln  -i  -s  $PWD/nvim/syntax/mkfile.vim          ~/.config/nvim/syntax/mkfile.vim
ln  -i  -s  $PWD/profile                         ~/.profile
ln  -i  -s  $PWD/scripts/active_shot.sh          ~/opt/bin/active_shot.sh
ln  -i  -s  $PWD/scripts/gws_cwd.sh              ~/opt/bin/gws_cwd
ln  -i  -s  $PWD/scripts/heaps.sh                ~/opt/bin/heaps.sh
ln  -i  -s  $PWD/scripts/highlight.zsh           ~/opt/bin/highlight
ln  -i  -s  $PWD/scripts/map_wacom_to_hdmi-0.sh  ~/opt/bin/map_wacom_to_hdmi-0.sh
ln  -i  -s  $PWD/scripts/nvimedit.sh             ~/opt/bin/nvimedit.sh
ln  -i  -s  $PWD/scripts/reload-browser          ~/opt/bin/reload-browser
ln  -i  -s  $PWD/scripts/remindme                ~/opt/bin/remindme
ln  -i  -s  $PWD/scripts/sel_shot.sh             ~/opt/bin/sel_shot.sh
ln  -i  -s  $PWD/scripts/shot.sh                 ~/opt/bin/shot.sh
ln  -i  -s  $PWD/scripts/unatlas.sh              ~/opt/bin/unatlas
ln  -i  -s  $PWD/scripts/yt                      ~/opt/bin/yt
ln  -i  -s  $PWD/scripts/yta                     ~/opt/bin/yta
ln  -i  -s  $PWD/scripts/ytrss.sh                ~/opt/bin/ytrss
ln  -i  -s  $PWD/zshrc                           ~/.zshrc
