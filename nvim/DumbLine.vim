" File: DumbLine.vim
" Author: oddlydrawn

function! DumbLine#UpdateVars() abort
	call s:GcmNeeded()
	call s:LinesAdded()
	call s:LinesDeleted()
	call s:ShortStatus()
endfunction

" TODO a separate function to force update b:dumbline_git_file_exists after a write,
" when var is false?
function! s:GitFileExists() abort
	if exists('b:dumbline_git_file_exists')
		return b:dumbline_git_file_exists
	endif
	if len(bufname('%')) > 0
		let b:dumbline_git_file_exists = len(system('git ls-files ' . bufname('%') . ' 2>/dev/null'))
	else
		let b:dumbline_git_file_exists = 0
	endif
	return b:dumbline_git_file_exists
endfunction

function! s:GcmNeeded() abort
	let b:dumbline_gcm_needed = v:false
	if isdirectory('.git') && s:GitFileExists()
		let l:ret = system('git diff --cached --quiet ' . bufname('%') . ' || echo mewtwo')
		if len(l:ret) > 1
			let b:dumbline_gcm_needed = v:true
		endif
	endif
endfunction

function! DumbLine#IsGcmNeeded() abort
	return b:dumbline_gcm_needed
endfunction

function! s:ShortStatus() abort
	let b:dumbline_short_status = '  '
	if isdirectory('.git') && s:GitFileExists()
		let l:xy = system('git status --short ' . bufname('%') . ' | cut -b1,2')
		let l:x = strcharpart(l:xy, 0, 1)
		let l:y = strcharpart(l:xy, 1, 1)
		if l:x ==# ' '
			let l:x = '_'
		endif
		if l:y ==# ' '
			let l:y = '_'
		endif
		let l:file_status = l:x . l:y
		if l:file_status !=# 'f' && l:file_status !=# '?'
			let b:dumbline_short_status = l:file_status
		endif
	endif
endfunction

function! DumbLine#GetShortStatus() abort
	return b:dumbline_short_status
endfunction

function! s:LinesAdded() abort
	let b:dumbline_lines_added = ''
	if isdirectory('.git') && s:GitFileExists()
		let l:lines_added = str2nr(system('git diff --numstat ' . bufname('%'). ' | cut -f1'))
		let b:dumbline_lines_added = '+' . l:lines_added
	endif
endfunction

function! DumbLine#GetLinesAdded() abort
	return b:dumbline_lines_added
endfunction

function! s:LinesDeleted() abort
	let b:dumbline_lines_deleted = ''
	if isdirectory('.git') && s:GitFileExists()
		let l:lines_deleted = system('git diff --numstat ' . bufname('%'). ' | cut -f2')
		let b:dumbline_lines_deleted = '-' . str2nr(l:lines_deleted)
	endif
endfunction

function! DumbLine#GetLinesDeleted() abort
	return b:dumbline_lines_deleted
endfunction


