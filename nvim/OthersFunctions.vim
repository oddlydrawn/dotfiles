" File: OthersFunctions.vim
" Author: oddlydrawn

" Remove trailing whitespace
" http://sartak.org/2011/03/end-of-line-whitespace-in-vim.html
function! OthersFunctions#StripTrailingWhitespace()
	" Preparation: save last search, and cursor position.
	let l:_s=@/
	let l:l = line('.')
	let l:c = col('.')
	" Do the business:
	%s/\s\+$//e
	" Clean up: restore previous search history, and cursor position
	let @/=l:_s
	call cursor(l:l, l:c)
endfunction

function OthersFunctions#Undotree_CustomMap()
	nmap <buffer> J <plug>UndotreeGoNextState
	nmap <buffer> K <plug>UndotreeGoPreviousState
endfunc

function! OthersFunctions#goyo_enter()
	ALEDisable
	set laststatus=0
	set linebreak
	set linespace=7
	set nocursorcolumn
	set noshowcmd
	set noshowmode
	set scrolloff=999
	set textwidth=80
	Limelight
	redraw
	redraws
endfunction

function! OthersFunctions#goyo_leave()
	set cursorcolumn
	set laststatus=2
	set linespace=0
	set nolinebreak
	set scrolloff=5
	set showcmd
	set showmode
	set textwidth=0
	ALEDisable
	Limelight!
	redraw
	redraws
endfunction

function! OthersFunctions#ExecuteInShell(command)
	" http://vim.wikia.com/wiki/Display_output_of_shell_commands_in_new_window
	let l:title = '__term_output__'
	let l:prev_window = winnr()
	let l:command = join(map(split(a:command), 'expand(v:val)'))
	let l:winnr = bufwinnr('^' . l:title . '$')
	silent! execute  l:winnr < 0 ? 'botright vnew ' . fnameescape(l:title) : l:winnr . 'wincmd w'
	setlocal buftype=nowrite bufhidden=wipe nobuflisted noswapfile nowrap nonumber norelativenumber filetype=diff
	echo 'Execute ' . l:command . '...'
	silent! execute 'silent %!'. l:command
	silent! execute 'vertical resize 80'
	silent! redraw
	silent! execute 'au BufUnload <buffer> execute bufwinnr(' . bufnr('#') . ') . ''wincmd w'''
	echo 'Shell command ' . l:command . ' executed.'
	silent! execute l:prev_window . 'wincmd w'
endfunction

function! Get_visual_selection()
    "https://stackoverflow.com/questions/1533565/how-to-get-visually-selected-text-in-vimscript#6271254
    " Why is this not a built-in Vim script function?!
    let [l:line_start, l:column_start] = getpos("'<")[1:2]
    let [l:line_end, l:column_end] = getpos("'>")[1:2]
    let l:lines = getline(l:line_start, l:line_end)
    if len(l:lines) == 0
        return ''
    endif
    let l:lines[-1] = l:lines[-1][: l:column_end - (&selection ==? 'inclusive' ? 1 : 2)]
    let l:lines[0] = l:lines[0][l:column_start - 1:]
    return join(l:lines, "\n")
endfunction

function! OthersFunctions#SetVisualSelection() abort
	let g:visual_selection=Get_visual_selection()
endfunction


