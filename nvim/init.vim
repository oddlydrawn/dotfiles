" File: init.vim
" Author: oddlydrawn
" Description: an organized mess

call plug#begin('~/.local/share/nvim/plugged')

Plug 'Raimondi/delimitMate'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'Shougo/neco-syntax', { 'for': 'vim' }
Plug 'Shougo/neco-vim', { 'for': 'vim' }
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
Plug 'airblade/vim-gitgutter'
Plug 'asciidoc/vim-asciidoc', { 'for': ['asciidoc', 'text'] }
Plug 'davidbeckingsale/writegood.vim'
Plug 'farmergreg/vim-lastplace'
Plug 'flazz/vim-colorschemes'
Plug 'jdonaldson/writeGooder'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/goyo.vim', { 'on': 'Goyo' }
Plug 'junegunn/limelight.vim', { 'on': 'Limelight' }
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'junegunn/vim-online-thesaurus'
Plug 'kana/vim-operator-user'
Plug 'ludovicchabant/vim-gutentags'
Plug 'machakann/vim-highlightedyank'
Plug 'majutsushi/tagbar', { 'on': 'TagbarToggle' }
Plug 'mbbill/undotree', { 'on': 'UndotreeToggle' }
Plug 'mhinz/vim-startify'
Plug 'nelstrom/vim-visual-star-search'
Plug 'racer-rust/vim-racer', { 'for': 'rust' }
Plug 'rhysd/vim-operator-surround'
Plug 'rust-lang/rust.vim', { 'for': 'rust' }
Plug 'sebastianmarkow/deoplete-rust', { 'for': 'rust' }
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-obsession'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-unimpaired'
Plug 'unblevable/quick-scope'
Plug 'w0rp/ale'
Plug 'wellle/targets.vim'

Plug 'gregsexton/gitv', { 'tag': '*', 'on': ['Gitv'] }
Plug 'inkarkat/vim-PatternsOnText'
Plug 'inkarkat/vim-ingo-library'
Plug 'jackiekircher/vim-chip8', { 'for': '8o' }
Plug 'milkypostman/vim-togglelist'
Plug 'zchee/deoplete-clang', { 'for': 'c' }
Plug 'Shougo/neoinclude.vim', { 'for': 'c' }
Plug 'chriskempson/base16-vim'
Plug 'jdonaldson/vaxe'
Plug 'skywind3000/gutentags_plus'

Plug 'tpope/vim-speeddating'
Plug 'Konfekt/FastFold'
Plug 'zhimsel/vim-stay'
Plug 'thalesmello/tabfold'

call plug#end()

set autoread
set autowrite
set backupdir=~/.config/nvim/backup,/tmp
set cmdheight=2
set colorcolumn=+1
set completeopt=menuone,preview
set confirm
set diffopt=filler,vertical,context:10
set directory=~/.config/nvim/swap,/tmp
set gdefault
set hidden
set ignorecase
set inccommand=split
set lazyredraw
set nofoldenable
set nojoinspaces
set nostartofline
set number
set path=.,**
set relativenumber
set runtimepath+=$PWD
set scrolloff=5
set shellpipe=>
set smartcase
set termguicolors
set undodir=~/.config/nvim/undodir
set undofile
set updatetime=220
set virtualedit=block
set winheight=40
set winwidth=55
set foldmethod=indent
set foldlevelstart=0

set path+=/opt/devkitpro/libctru/include

if executable('rg')
	set grepprg=rg\ --vimgrep\ $*
	set grepformat=%f:%l:%c:%m
endif

set statusline=
set statusline+=%{DumbLine#GetShortStatus()}
set statusline+=\ %t\ \ %n\ \ %m
set statusline+=%=
set statusline+=\ %{ObsessionStatus('[$]','[p]')}
set statusline+=\ %2*%{DumbLine#GetLinesAdded()}%0*
set statusline+=\ %3*%{DumbLine#GetLinesDeleted()}%0*
set statusline+=\ %{fugitive#head()}
set statusline+=\ %4*%{DumbLine#IsGcmNeeded()?'-[GCM]-':''}%0*
set statusline+=%=%<
set statusline+=%5*%{&spell?'[SPELL]':''}%0*
set statusline+=%y
set statusline+=%q
set statusline+=%5*%r%0*%w
set statusline+=\ [%{&fileformat}]
set statusline+=\ %-14.(%l,%c%V%)\ %P

command! -complete=shellcmd -nargs=+ Shell call OthersFunctions#ExecuteInShell(<q-args>)
command! -nargs=+ Gh execute 'helpgrep <args>' | copen 30 | cfirst | redraw!
command! -nargs=+ Rg execute 'silent! grep! <args>' | copen 30 | cfirst | redraw!
command! -nargs=+ RgBuffer execute 'Rg <args> %'

augroup CrammingHole
	autocmd!
	autocmd FileType * RainbowParentheses
	autocmd VimEnter,BufEnter,BufAdd,BufWinEnter,BufWritePost,CmdwinEnter * call DumbLine#UpdateVars()
	autocmd User GitGutter call DumbLine#UpdateVars()
	autocmd VimEnter * nested :call MyFunctions#LoadSession()

	autocmd BufWritePre * call OthersFunctions#StripTrailingWhitespace()

	autocmd! User GoyoEnter nested call OthersFunctions#goyo_enter()
	autocmd! User GoyoLeave nested call OthersFunctions#goyo_leave()
	autocmd FileType gitcommit setlocal spell
	autocmd FileType fzf tnoremap <Esc> <C-\><C-n>:close<CR>
        autocmd FileType 8o set commentstring=#%s
        autocmd FileType 8o let b:loaded_delimitMate = 1
augroup End

augroup AsciiDoc
	autocmd!
	autocmd FileType asciidoc,text nested :WritegoodEnable
	autocmd FileType asciidoc,text nnoremap <Space>m :silent! make <CR>
	autocmd FileType asciidoc,text nnoremap <Space>r :silent! !firefox %:h/%:t:r'.html'<CR>
	autocmd FileType asciidoc,text set makeprg=asciidoctor\ %
	autocmd FileType asciidoc,text setlocal textwidth=80 spell
augroup End

augroup Haxe
	autocmd!
	" autocmd FileType haxe nnoremap <Space>r :silent! !firefox index.html<CR>
	autocmd FileType haxe nnoremap <Space>r :silent! !hl /tmp/game.hl<CR>
	autocmd FileType haxe nnoremap <Space>m mZ:silent! !haxe hl.hxml 2> //tmp/haxeerr.txt<CR>:cfile /tmp/haxeerr.txt<CR>:cwindow<CR>:cfirst<CR>:redraw<CR>
	autocmd FileType haxe set formatprg=haxelib\ run\ formatter\ -s\ src
augroup End

augroup Rust
	autocmd!
	autocmd FileType rust nnoremap <Space>r :RustRun<CR>
	autocmd FileType rust nnoremap <Space>m :Shell cargo build<CR>
	autocmd FileType rust nnoremap <Space>t :Shell cargo test<CR>
	autocmd FileType rust nnoremap <F5> :Shell cargo +nightly clippy --
				\ -Dclippy
				\ -Dclippy_pedantic
				\ -Dmissing_debug_implementations
				\ -Dmissing_copy_implementations
				\ -Dtrivial_casts
				\ -Dtrivial_numeric_casts
				\ -Dunsafe_code
				\ -Dunused_import_braces
				\ -Dunused_qualifications
				\ <CR>
	autocmd Filetype rust nmap gs <Plug>DeopleteRustGoToDefinitionSplit
	autocmd Filetype rust nmap gx <Plug>DeopleteRustGoToDefinitionVSplit
	autocmd FileType rust set makeprg=cargo\ build
	autocmd FileType rust let b:delimitMate_quotes = '"'
	autocmd FileType rust nnoremap <localleader>d yiwoprintln!("The value of <Esc>pA is: {}", <Esc>pA);<Esc>
	autocmd FileType rust nnoremap <localleader>f yiwoprintln!("The value of <Esc>pA is: {:?}", <Esc>pA);<Esc>
	autocmd FileType rust nnoremap <localleader>g yiwoprintln!("The value of <Esc>pA is: {:#?}", <Esc>pA);<Esc>
	autocmd FileType rust nnoremap <localleader>t yiwolet xxx = <Esc>pA.whatisthis();<Esc>
augroup End

augroup StatusLineColors
	autocmd!
	autocmd  ColorScheme  *  highlight  StatusLineNC  ctermbg=234  ctermfg=242
	autocmd  ColorScheme  *  highlight  StatusLine    ctermbg=240  ctermfg=250        cterm=bold
	autocmd  ColorScheme  *  highlight  User1         ctermbg=108  ctermfg=234        cterm=bold
	autocmd  ColorScheme  *  highlight  User2         ctermbg=240  ctermfg=blue       cterm=bold
	autocmd  ColorScheme  *  highlight  User2         ctermbg=240  ctermfg=108        cterm=bold
	autocmd  ColorScheme  *  highlight  User3         ctermbg=240  ctermfg=red        cterm=bold
	autocmd  ColorScheme  *  highlight  User4         ctermbg=208  ctermfg=234        cterm=bold
	autocmd  ColorScheme  *  highlight  User5         ctermbg=240  ctermfg=darkred    cterm=bold
	autocmd  ColorScheme  *  highlight  ColorColumn   ctermbg=238  ctermfg=250
augroup End

colorscheme base16-ocean

inoremap ! !<C-g>u
inoremap . .<C-g>u
inoremap : :<C-g>u
inoremap ; ;<C-g>u
inoremap <C-f> <C-o>j
inoremap <C-l> <C-o>A
inoremap <expr><C-h> deoplete#smart_close_popup()."\<C-h>"
inoremap <expr><S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr><Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap ? ?<C-g>u
map <silent>sa <Plug>(operator-surround-append)
map <silent>sd <Plug>(operator-surround-delete)
map <silent>sr <Plug>(operator-surround-replace)
nmap [g :Gstatus<CR><C-P><CR>gg]c
nmap ]g :Gstatus<CR><C-N><CR>gg]c
nmap gyc yygccp
nnoremap <C-h> <C-w>h<C-w>=
nnoremap <C-j> <C-w>j<C-w>=
nnoremap <C-k> <C-w>k<C-w>=
nnoremap <C-l> <C-w>l<C-w>=
nnoremap <F1> va}
nnoremap <F2> :TagbarToggle<Cr>
nnoremap <F4> :Vexplore<cr>
nnoremap <Space>= <C-w>=
nnoremap <Space>A mzgg0VG
nnoremap <Space>G :Goyo<CR>
nnoremap <Space>N :NERDTreeToggle<CR>
nnoremap <Space>O :silent! tabonly<CR>:only<CR>
nnoremap <Space>U :UndotreeToggle<CR>
nnoremap <Space>a :ALEToggle<CR>
nnoremap <Space>b :split<CR><C-w>=
nnoremap <Space>c :close<CR><C-w>=
nnoremap <Space>ccl :cclose<CR>:lclose<CR><C-w>=
nnoremap <Space>cd :cd %:p:h<CR>:Gcd<CR>:pwd<CR>
nnoremap <Space>ct mzvip:!column -t<CR>`z
nnoremap <Space>cw mz:cwindow 10<Cr>:cfirst<Cr>
nnoremap <Space>f mzgg0VGgq`z
nnoremap <Space>gb :RgBuffer<Space>
nnoremap <Space>gh :Gh<space>
nnoremap <Space>l mz:lopen 10<CR>:lfirst<CR>
nnoremap <Space>m mz:make<CR>
nnoremap <Space>o :only<CR>
nnoremap <Space>p "+p
nnoremap <Space>ps mzvip:sort<CR>`z
nnoremap <Space>q :qall<cr>
nnoremap <Space>rg :Rg<Space>
nnoremap <Space>u :update<CR>:nohlsearch<CR>:redraw!<CR>
nnoremap <Space>v :vsplit<CR><C-w>=
nnoremap <Space>vrc :vsplit<CR>:e ~/.config/nvim/init.vim<CR><C-w>=
nnoremap <Space>xm :!xmodmap ~/.Xmodmap<CR>
nnoremap <Space>y "+y
nnoremap <c-p> :FZF<CR>
nnoremap <silent> <BS> :nohlsearch<CR>
nnoremap <silent> <Space>s :set spell!<CR>
nnoremap <silent> [W :lfirst<CR>
nnoremap <silent> [w :lprevious<CR>
nnoremap <silent> ]W :llast<CR>
nnoremap <silent> ]w :lnext<CR>
nnoremap N Nzz
nnoremap Q @q
nnoremap n nzz
noremap Y y$
tnoremap <C-h> <C-\><C-N><C-w>h<C-w>=
tnoremap <C-j> <C-\><C-N><C-w>j<C-w>=
tnoremap <C-k> <C-\><C-N><C-w>k<C-w>=
tnoremap <C-l> <C-\><C-N><C-w>l<C-w>=
tnoremap <Esc> <C-\><C-n>
xmap gyc ygvgc`>p
xnoremap < <gv
xnoremap > >gv

xnoremap <Space>e :.w !bash<CR>

nnoremap <Space># :%s/<C-r><C-w>/
xnoremap <Space># :call OthersFunctions#SetVisualSelection()<CR>
			\ :%s/<C-r>=g:visual_selection<CR>/
nnoremap <Space><Space> :let cursor_word='<C-r><C-w>'<CR>
			\ :silent! grep <C-r>=cursor_word<CR><CR>
			\ :copen 40<CR>
			\ :redraw!<CR>
			\ :cdo s/<C-r>=cursor_word<CR>/
xnoremap <Space><Space> :call OthersFunctions#SetVisualSelection()<CR>
			\ :silent! grep <C-r>=g:visual_selection<CR><CR>
			\ :copen 40<CR>
			\ :redraw!<CR>
			\ :cdo s/<C-r>=g:visual_selection<CR>/

xnoremap <Space>- :s/+\v//e<CR>
xnoremap <Space>+ :s/+\v//e<CR>
			\ :*s/\s\+$//e<CR>
			\ gvgq
			\ :*s/\n/ + \r/e<CR>
			\ :nohlsearch<Cr>

"Commit (c)
nnoremap <Space>gcm :Gcommit<CR>i

"Index (i)
nnoremap <Space>gid :Shell git diff --cached --no-ext-diff --unified=0<CR>
nnoremap <Space>giD :Shell git diff --cached --word-diff=plain --no-ext-diff --unified=0<CR>

"Working Copy (w)
nnoremap <Space>gws :Shell git status --short<CR>
nnoremap <Space>gwS :Shell git status<CR>
nnoremap <Space>gwd :Shell git diff --no-ext-diff --unified=0<CR>
nnoremap <Space>gwD :Shell git diff --word-diff=plain --no-ext-diff --unified=0<CR>

let g:mapleader=','
let g:maplocalleader='\'

let g:ale_echo_delay = 220
let g:ale_echo_msg_error_str='E'
let g:ale_echo_msg_format = '[%linter%: %severity%] %code: %%s'
let g:ale_echo_msg_info_str='I'
let g:ale_echo_msg_warning_str='W'
let g:ale_lint_delay = 2400
let g:ale_linters = { 'rust': ['rls', 'cargo', 'rustfmt'],
			\ 'zsh': ['shell', 'shellcheck'
			\ ]
			\ }
let g:ale_sh_shell_default_shell = 'zsh'
let g:ale_sign_column_always = 1

let delimitMate_balance_matchpairs = 1
let delimitMate_expand_cr = 2
let delimitMate_expand_space = 1
let delimitMate_jump_expansion = 1

call deoplete#custom#source('_', 'converters', ['converter_remove_paren'])
call deoplete#custom#source('ultisnips', 'matchers', ['matcher_fuzzy'])
call deoplete#custom#source('racer', 'rank', 25)
call deoplete#custom#source('Rust', 'rank', 125)
let g:deoplete#auto_complete_start_length = 1
let g:deoplete#enable_at_startup = 1
let g:deoplete#enable_smart_case = 1
let g:deoplete#tag#cache_limit_size=10000000
let g:deoplete#sources#clang#libclang_path = "/usr/lib/llvm-6.0/lib/libclang.so.1"
let g:deoplete#sources#clang#clang_header = "/usr/lib/llvm-6.0/lib/clang/"

let g:deoplete#sources#rust#racer_binary='/home/oddlydrawn/.cargo/bin/racer'
let g:deoplete#sources#rust#rust_source_path='$(rustc --print sysroot)/lib/rustlib/src/rust/src'

let g:ftplugin_rust_source_path = '$(rustc --print sysroot)/lib/rustlib/src/rust/src'

let g:highlightedyank_highlight_duration = 800

let g:limelight_conceal_ctermfg = 235
let g:limelight_conceal_guifg = '#262626'

let g:netrw_bufsettings='nomodifiable nomodified nobuflisted nowrap readonly relativenumber'
let g:netrw_liststyle = 3
let g:netrw_preview   = 1

let g:python_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python3'

let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

let g:racer_command = '~/.cargo/bin/racer'
let g:racer_experimental_completer = 1

let g:rainbow#max_level = 16
let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['{', '}'], ['<', '>']]

let g:rustfmt_autosave = 1
let g:rustfmt_command = 'rustup run nightly rustfmt'

let g:startify_session_dir = '~/.local/share/nvim/session'
let g:startify_bookmarks = [
			\ '~/Documents/asciidoc/notes/ascii_index.txt',
			\ '~/opt/git/dotfiles/init.vim',
			\ ]

let g:tagbar_compact = 1
let g:tagbar_sort = 0
let g:tagbar_width = 25

let g:tagbar_type_rust = {
			\ 'ctagstype' : 'rust',
			\ 'kinds' : [
			\'T:types,type definitions',
			\'f:functions,function definitions',
			\'g:enum,enumeration names',
			\'s:structure names',
			\'m:modules,module names',
			\'c:consts,static constants',
			\'t:traits',
			\'i:impls,trait implementations',
			\]
			\}

let g:UltiSnipsEditSplit='vertical'
let g:UltiSnipsExpandTrigger='<c-h>'
let g:UltiSnipsJumpBackwardTrigger='<c-k>'
let g:UltiSnipsJumpForwardTrigger='<c-j>'

let g:undotree_SetFocusWhenToggle = 1
let g:undotree_WindowLayout = 2


" let g:neoinclude#_paths = {
" 			\ '/usr/include/',
" 			\ '/opt/devkitpro/libctru/include',
" 			\ }


"/gutentags_plus
" enable gtags module
let g:gutentags_modules = ['ctags', 'gtags_cscope']
" config project root markers.
let g:gutentags_project_root = ['.git']
" forbid gutentags adding gtags databases
let g:gutentags_auto_add_gtags_cscope = 0

nmap <F5> <Plug>(FastFoldUpdate)
nmap zuz <Plug>(FastFoldUpdate)
let g:fastfold_savehook = 1
let g:fastfold_fold_command_suffixes =  ['x','X','a','A','o','O','c','C']
let g:fastfold_fold_movement_commands = [']z', '[z', 'zj', 'zk']

let g:markdown_folding = 1
let g:vimsyn_folding = 'af'
let g:sh_fold_enabled= 7
let g:perl_fold = 1
let g:perl_fold_blocks = 1
let g:rust_fold = 1
let g:fastfold_skip_filetypes = [ 'taglist' ]
