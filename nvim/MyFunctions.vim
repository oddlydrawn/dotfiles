" File: MyFunctions.vim
" Author: oddlydrawn

function! MyFunctions#LoadSession() abort
	if filereadable('Session.vim')
		exe 'source Session.vim'
	endif
endfunction

