#!/bin/sh
# File: shot.sh
# Description: Takes a screenshot (with maim) of entire desktop with:
#   timestamp and desktop resolution in the file name.
# Author: oddlydrawn
# Last Modified: 2016-04-16
# Example output:
#    2016-04-14-17:59:42-3286x1080-maim-desktop.png

DATE=`date +%F-%T`
RES=`xrandr | head -1 | awk '{ print $8 $9 $10 }' | sed 's/,//'`
SHOT_DIR=~/Pictures/screenshots
SHOT_NAME="$SHOT_DIR/$DATE-$RES-maim-desktop.png"

maim $SHOT_NAME

