#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Requires python 3, pyperclip, bs4 (BeautifulSoup 4), requests
# As of python 3.7.0, can be run using pipenv:
#   $ pipenv run ytrsslink.py

from bs4 import BeautifulSoup
import pyperclip
import requests
import sys

def main():
    """ Uses a youtube link from the clipboard, prints a youtube rss url"""

    # TODO - find an rss reader that uses a plain text file to read feeds
    # from so i could just append that file from this thing, which also
    # understands youtube rss feeds

    yt_url = pyperclip.paste()
    soup = get_soup(yt_url)

    name = soup.find(itemprop="name").get("content")
    description = soup.find(itemprop="description").get("content")
    channel_id = soup.find(itemprop="channelId").get("content")
    url = yt_url
    youtube_rss = "https://www.youtube.com/feeds/videos.xml?channel_id=" + channel_id

    print(name)
    print(description)
    print(url)
    print(channel_id)
    print(youtube_rss)
    sys.exit()


def get_soup(url):
    """ Get the content from a url

    :url: should be a youtube url
    :returns: soup so beautiful it'll make you cry, or so I've heard

    """
    response = requests.get(url)
    if (response.status_code == requests.codes.ok):
        return BeautifulSoup(response.text, 'html.parser')
    else:
        print("Bad request :( to {} : {}".format(url, response.status_code))
        print("Also, this: {}".format(response))
        sys.exit(1)


if __name__ == "__main__":
    main()
