#!/bin/bash

if (( $# != 1 )); then
        echo "Usage: heaps [PROJECT]"
        echo "Clones bare heaps repo into PROJECT directory. Also initializes local git repo."
        echo ""
        echo "Requires one argument, clones bare heaps repo, initializes bare git repo, edits"
        echo "PROJECT/.git/config to reflect newly created git repo url"
        echo "                            "
        echo "  PROJECT,                  The project directory to create"
        echo ""
        echo "Examples:"
        echo "  heaps test      Clones bare repo into test, initializes a bare git repo named"
        echo "                  heaps_test in /mnt/git, updates .git/config to reflect the"
        echo "                  change, and you're ready to play"
        echo ""
        exit
fi

PROJECT=$1; export PROJECT
GITLOC=/mnt/git/heaps_$PROJECT.git; export GITLOC

cp -r bare $PROJECT
# rm -rf $PROJECT/.git
git init --bare $GITLOC
perl -i -pe 's/\w*url = .*/url = $ENV{GITLOC}/g;' $PROJECT/.git/config
