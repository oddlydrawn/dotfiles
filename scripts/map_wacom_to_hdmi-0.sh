#!/bin/bash
WACOM_ID=`xsetwacom --list | grep stylus | cut -f 2 | cut -d ' ' -f 2`
MONITOR=`xrandr --listmonitors | tail -n1 | cut -f6 -d' '`

xsetwacom --set $WACOM_ID MapToOutput $MONITOR
