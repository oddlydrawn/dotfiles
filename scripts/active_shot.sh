#!/bin/sh
# File: active_shot.sh
# Description: Takes a screenshot (with maim) of the active window with:
#   timestamp, active window size, class name of window, and optionally the
#   class name in the screenshot file name.
# Author: oddlydrawn
# Last Modified: 2016-04-16
# Example output:
#    2016-04-14-16:43:56-1916x505-maim-Roxterm_active_shot_sh_opt_bin_-_VIM.png
# or:
#    2016-04-14-16:43:56-1916x505-maim-Roxterm.png

WIN_ID=`xdotool getactivewindow`
DATE=`date +%F-%T`
WIN_CLASS=`xprop -id $WIN_ID WM_CLASS | awk '{ print $4 }' | tr -cd '[[:alnum:]]'`
WIN_NAME=`xprop -id $WIN_ID WM_NAME | awk '{ $1=$2=""; print $0 }' | tr -c '[[:alnum:]]_-' '_' | sed 's/_\+/_/g;s/_\$//'`
SIZE=`xwininfo -id $WIN_ID | grep geometry | awk '{ print $2 }' | cut -d "-" -f1`
SHOT_DIR=~/Pictures/screenshots

SHOT_NAME_WITH_WIN_NAME="$SHOT_DIR/$DATE-$SIZE-maim-$WIN_CLASS$WIN_NAME.png"
SHOT_NAME_JUST_CLASS="$SHOT_DIR/$DATE-$SIZE-maim-$WIN_CLASS.png"

# Select one or the other
SHOT_NAME=$SHOT_NAME_WITH_WIN_NAME
# SHOT_NAME=$SHOT_NAME_JUST_CLASS

maim -i $WIN_ID $SHOT_NAME

