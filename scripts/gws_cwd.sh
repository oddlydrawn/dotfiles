#!/bin/bash

find * -maxdepth 0 -type d -print -exec sh -c '(cd {} && git status --porcelain && echo)' \;

