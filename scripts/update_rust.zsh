rustup self update
# rustup update nightly

if [[ -e "${HOME}/.cargo/bin/rls" ]]
then
	echo "rls found"
	rustup component add rls-preview
	rustup component add rust-analysis
	rustup component add rust-src
else
	echo "rls not found"
	# rustup component add rls-preview --toolchain nightly
	# rustup component add rust-analysis --toolchain nightly
	# rustup component add rust-src --toolchain nightly

	rustup component add rls-preview
	rustup component add rust-analysis
	rustup component add rust-src
fi

cargo install racer
//oops
cargo install rustfmt-nightly
