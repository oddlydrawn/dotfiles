#!/bin/bash

if (( $# != 2 )); then
	echo "Usage: unatlas [ATLAS] [SIZE]"
	echo "Creates sprites from an atlas"
	echo ""
	echo "Requires two arguments, the atlas to convert into tiles, the tile size. Dumps"
	echo "sprites into 'out' directory"
	echo "                            "
	echo "  ATLAS,                    the atlas png"
	echo "  SIZE,                     tile size"
	echo ""
	echo "Examples:"
	echo "  unatlas atlas_sheet.png 32x32"
	echo "  unatlas atlas_sheet.png"
	echo ""
	exit
fi

ATLAS=$1
SIZE=$2
ATLAS_NO_EXT="${ATLAS%.*}_"

mkdir out
convert $ATLAS +gravity -crop $SIZE +repage +adjoin out/$ATLAS_NO_EXT%03d.png

