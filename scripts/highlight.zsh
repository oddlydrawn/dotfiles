#!/usr/bin/zsh
# found this here:
# https://softwarerecs.stackexchange.com/questions/14413/grep-that-highlights-instead-of-filter
PATTERN=$1
FILE=$2

grep -E "^|$PATTERN" --color='always' $FILE
