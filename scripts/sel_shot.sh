#!/bin/sh
# File: sel_shot.sh
# Description: Takes a screenshot selection with the geometry from slop (and
# maim) with:
#    timestamp and selection size in the file name.
# Author: oddlydrawn
# Last Modified: 2016-04-16
# Example output:
#	  2016-04-14-17:59:32-782x354-maim-selection.png

DATE=`date +%F-%T`
# GEOMETRY=`slop | grep G | sed 's/G=//'`
GEOMETRY=`slop`
SIZE=`echo $GEOMETRY | cut -d "+" -f1`
SHOT_DIR=~/Pictures/screenshots
SHOT_NAME="$SHOT_DIR/$DATE-$SIZE-maim-selection.png"

maim -g $GEOMETRY $SHOT_NAME

