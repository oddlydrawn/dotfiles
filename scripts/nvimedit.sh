#!/bin/bash
# https://www.reddit.com/r/vim/comments/7pmv3d/workflows_that_work/dsnrrv1/
_INPUT_FILE=$(mktemp)
# i3 will make this a scratch window based on the class.
# i3-sensible-terminal -c "scratch-i3-input-window" nvim -c "set noswapfile" "$_INPUT_FILE"
# i3-sensible-terminal -e nvim -c "set noswapfile" -c "set filetype=text" -c "set fileformat=dos" "$_INPUT_FILE"
konsole -e nvim -c "set noswapfile" -c "set filetype=text" -c "set fileformat=dos" "$_INPUT_FILE"
sleep 2
xdotool type --clearmodifiers --delay 0 --file $_INPUT_FILE
rm $_INPUT_FILE
