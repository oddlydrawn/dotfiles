// ==UserScript==
// @name     Unnamed Script 361705
// @version  1
// @grant    none
// ==/UserScript==
function clamp_0_255(value) {
  if (value < 0) {
    return 0;
  }
  if (value > 255) {
    return 255;
  }
  return value;
}

function bg_is_white(r, g, b) {
  return r == 255 && g == 255 && b == 255;
}

// Thank you internet T-T
// https://userscripts-mirror.org/topics/30513
function darken(colour) {
  var rgb_array = colour.split(',');
  if (rgb_array.length != 3)
    return colour;
  var r = rgb_array[0].replace('rgb(','');
  var g = rgb_array[1];
  var b = rgb_array[2].replace(')','');

  if bg_is_white(r, g, b) {
    // cream
    r = 244;
    g = 236;
    b = 216;
  } else {
    // darken
    var factor = 0.94;

    var red = clamp_0_255(Math.round(parseInt(r) * factor));
    var green = clamp_0_255(Math.round(parseInt(g) * factor));
    var blue = clamp_0_255(Math.round(parseInt(b) * factor));
  }
  return 'rgb(' + red + ',' + green + ',' + blue + ')';
}

window.runScript = function() {
  var doc_bg = document.defaultView.getComputedStyle(document.body, null).getPropertyValue("background-color");
  document.body.style.background = darken(doc_bg);

  var everything = document.getElementsByTagName("*");
  for(var i=0; i < everything.length; i++) {
    var bgcolor = document.defaultView.getComputedStyle(everything[i], "").getPropertyValue("background-color");
    everything[i].style.backgroundColor = darken(bgcolor);
  }
}
window.addEventListener('load', runScript, false);

